Bienvenue sur mon site !

Un site créé dans le cadre du cours _How To Make (almost) Any Experiment Using Digital Fabrication_ enseigné principalement par TERWAGNE Denis.

Commençons par la base des bases:


## Qui suis je ?


![photo passion](https://imagizer.imageshack.com/img922/2503/gSPGLD.png)

<span style = "color:blue">Yo!

 Moi c'est **Martin Gilles** (celui à gauche sur la photo). Comme tu peux le voir j'aime bien le ski et la montagne mais c'est pas tout!

Si tu veux en savoir un petit plus sur moi je t'invite à visiter mon nouveau site web dernière génération ❄️

## Mes aventures 

Alors pour faire court, je viens de Bruxelles, j'ai 22 ans et je suis actuellement en BAC3 à l'**EBB**.


Durant ces 22 belles années j'ai eu le temps de faire 6 ans d'escalade, de me casser le genoux, de faire du foot, de me casser la hanche puis de faire de la cuisine et me casser la croute lol.

J'ai aussi eu le temps de partir un an en Argentine où j'ai eu la chance d'explorer la Patagonie, cette partie du monde lointaine et méconnue qui m'a donné goût à l'exploration, à la curiosité et qui m'a guidé jusque sur les bancs de l'ULB où j'ai décidé qu'il était temps pour moi d'en savoir un peu plus sur la nature et les lois qui la gouvernaient.



## Mes projets  

Mis à part mes différents projets hors cursus, mon projet de groupe principal cette année était de créer un radeau végétal afin d'explorer le potentiel de ce genre de structure quant à l'assimilation des nutriments excessifs dans certains bassins d'eau douce. 

A l'heure où je vous parle celui-ci est actuellement dans l'eau et se porte comme un charme ! 
> _voir la photo ci dessous_

![test photo](https://imagizer.imageshack.com/v2/640x480q90/923/6PVc9U.jpg)
## La suite 

Après avoir fini cette année et avoir délivré un beau projet (je l'espère) dans le cadre de ce cours, mon idée est de repartir sur les terres argentines afin d'appliquer mes connaissances acquises durant ces trois dernières années de manière plus concrète. 
S'en suivra alors deux belles années de Master à Delft (si tout se passe bien) dans les domaines des sciences de la terre appliquées ou dans les énergies renouvelables, à voir encore. 

Voila c'est tout pour moi! 

Merci pour la lecture et bonne journée ☀️



