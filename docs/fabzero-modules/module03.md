# 3. Impression trois dés

Lors de ce 3ème module, nous avons été introduits aux imprimantes 3D.Durant module précédent, nous avons appris comment utiliser les différents logiciels permettant la conception de pièces 3D comme openscad ou freecad. Cette semaine, l'heure était à la création. 

## 3.1 Les imprimantes

Les imprimantes 3D sont apparues dans les années 1980 et
se sont développées jusqu'à pouvoir réaliser des objets                
de plus en plus utils et de plus en plus précis.
Leur fonctionnement est simple, grâce à une ou deux têtes d'impression,
les machines 3D permettent de déposer le matériau, 
alors sous forme de bobines de filament, sur un lit d'impression. 
Le filament est fondu et déposé couche par couche à l'aide d'une buse d'impression.

![impri3D](https://imagizer.imageshack.com/img922/7316/pkKoGu.gif)
             

### 3.1.1 Oui mais, 

Les imprimantes 3D, bien que très utiles et faciles à manipuler présentent aussi leurs **limites**. En effet, le processus dis "couche par couche" implique que l'objet se construit verticalement, de plus en plus haut. L'imprimante crée alors des supports afin d'éviter de construire dans le vide. Cependant, des objets construits trop à vide comme des ressorts impliquent énormément de supports et utilisent donc beaucoup plus de fils, de bobines dans le processus de création qu'il en faut pour créer l'objet lui-même.



Il y'a aussi des formes particulières qui posent problèmes à l'imprimante 3D, comme les sphères par exemple. En effet, les sphères sont créées en formant des "marches d'escaliers". Cependant, à une certaine hauteur, la gravité agit sur ces couches décalées les unes des autres et le risques que ces couches s'affaissent augmente de plus en plus. De plus, des erreurs d'impressions peuvent s'accumuler et rendre la paroi sphèrique plus rugueuse. La technique la plus efficace pour créer une sphère est alors de former deux hémisphères et de les assembler avec de la super glue.

>![prbl](https://imagizer.imageshack.com/v2/380x190q90/922/3kcdlN.jpg)
>![sphere](https://imagizer.imageshack.com/v2/380x190/924/jFgqu7.jpg) 


Cependant, avec une bonne manipulation des différents paramètres d'impression et du bon matériel, il est possible de créer de nombreux objets grâce à cet outil, en voici quelques exemples : 

>![swag](https://imagizer.imageshack.com/v2/640x480q90/924/RZqlN5.png)


## 3.2 PrusaSlicer 

PrusaSlicer est un logiciel open-source de découpage en tranches de fichiers 3D, qui a pour objectif de convertir un modèle en instructions utilisables par une imprimante 3D.
Une fois ouvert, le logiciel s'affiche de la manière suivante : 

> ![prsua](https://imagizer.imageshack.com/v2/800x600q90/922/RomDfz.png)


Au centre est représenté la plaque de construction, c'est sur celle-ci que se place notre objet une fois importé. 
Les cadres sur la photo soulignent les princiales fonctionnalités accessibles, en voici une rapide description : 

![test](https://imagizer.imageshack.com/v2/20x15q90/923/DcoeuF.png)  
Dans les 2 cadres rouges se trouvent différents outils qui permettent de manipuler l'objet que l'on souhaite imprimer. On peut par exemple appliquer des rotations à notre objet, choisir quelle face sera sur la plaque de construction ou encore simplement le déplacer sur cette même plaque [outils cadre de gauche]. On peut aussi rajouter des objets, en supprimer ou même séparer deux objets superposés. 

![bleu](https://imagizer.imageshack.com/v2/20x15/923/mwBlGV.png)  

Le cadre bleu met en évidence les différentes pages qui permettent d'avoir accès à des paramètres plus précis.

> * Réglages d'impression :

Permettent de régler la hauteur entre deux couches et donc la précision de l'impression, la hauteur basique est de ´0.2mm´, en fonction de la précision, le temps que prendra l'impression sera modifié. Il est aussi possible de régler le nombres de couches internes. Cela permet de gérer la solidité de l'objet. 


> * Réglages du filament :

Sont en général prédéterminés en fonction de la bobine utilisée. Il est cependant intéressant de garder un oeil sur la température du plateau et de la buse. En général, la température de la buse pour la première couche est supérieur de 5 degrés à la température du reste des couches afin d'assurer une meilleure adhérence. 


> * Réglages de l'imprimante :

Permettent de modifier la hauteur maximale d'impression ou encore la forme du plateau 


![vert](https://imagizer.imageshack.com/v2/20x15q90/923/ZlT1sg.png)  
Le cadre vert contient les paramètres principaux qui résument en quelque sorte ceux décrits ci-dessus. Il est important de mettre des supports en fonction de sa structure afin d'éviter que celle-ci ne travaille à vide.


![mauve](https://imagizer.imageshack.com/v2/20x15q90/923/DZidXF.png)  
Enfin, ce dernier cadre est vide car je n'ai encore importer aucun code dans le logiciel, cependant, il se remplira et affichera les différents fichiers qui composent la forme créée. 

### 3.2.2 De mon coté 

Afin d'importer mon ressort sur PrusaSlicer, je l'ai d'abord converti en fichier `STL` sur Openscad. Le format de fichier STL est un format utilisé dans les logiciels de stéréolithographie, c'est à dire les logiciels qui permettent l'impression de solide en trois dimensions. Je l'ai ensuite disposé sur le plan de travail de PrusaSlicer grâce à la fonctionnalité `petite cube +` se trouvant dans le cadre supérieur rouge. J'ai décidé de l'imprimer avec une précision de 2mm d'entre-couches et un remplissage de 25%. J'ai ensuite séléctionner _découper maintenant_. 

![prusse](https://imagizer.imageshack.com/v2/800x600q90/923/JL8XuY.png)

Il m'a ensuite suffit d'exporter le _G-code_ dans une carte sd qui sera ensuite lue par l'imprimante. Un _g-code_ est enfaite le nom donné à un fichier contenant la séquence d'instructions que suivra l'imprimante afin de former l'objet 3D.

## 3.3 L'impression (bonne ou mauvaise?)

Une fois la carte SD insérée, j'ai du séléctionner mon fichier et lancer l'impression. Il se peut que l'interval de temps entre le lancement et l'activation de la buse soit un peu long mais pas de panique, il suffit d'attendre quelques secondes de plus et l'impression devrait se lancer sans soucis. L'impression s'est passée sans soucis sous les yeux ébahis de [Eliot](https://gitlab.com/fablab-ulb/enseignements/2022-2023/fabzero-experiments/students/eliot.niedercorn) et [Suzanne](https://gitlab.com/fablab-ulb/enseignements/2022-2023/fabzero-experiments/students/suzanne.lipski). Deux collègues à moi très inspirés par mon travail. 

![découpe](https://imagizer.imageshack.com/v2/640x480q90/923/fMbJEy.jpg)


## 3.4 Flexort 

Le ressort est sorti en bon état et fonctionne !

>`petite démo`


![gif ressort](https://imagizer.imageshack.com/img924/6808/BPdcDI.gif)

> Si le gif ne fonctionne pas, vous pouvez suivre ce [lien youtube](https://youtube.com/shorts/nwl_2jxCw4o) qui connait un fort succès dans la communauté des flexlinker.

Cependant, lors d'un test de solidité effectué avec Eliot, probablement un peu jaloux, mon ressort s'est brisé tout comme la régularité de mes battements d'coeur. 

![cassé](https://imagizer.imageshack.com/v2/800x600q90/922/KU0R0x.png)

### 3.4.1 Modifications

J'ai donc appris de mes erreurs et modifié les attaches de mon ressort qui semblaient être la cause de sa fragilité. Voici mon ressort amélioré  :

![cssemoins](https://imagizer.imageshack.com/v2/480x340q90/923/ot4feH.png)

## 3.5 Kit 

Le kit a été formé avec les éléments créés par [Emma Dubois](https://gitlab.com/fablab-ulb/enseignements/2022-2023/fabzero-experiments/students/emma.dubois/-/tree/main/docs), [Suzanne Lipski](https://gitlab.com/fablab-ulb/enseignements/2022-2023/fabzero-experiments/students/suzanne.lipski) et [Eliot Niedercorn](https://gitlab.com/fablab-ulb/enseignements/2022-2023/fabzero-experiments/students/suzanne.lipski).

 L'idée a été de former une catapulte dont le mouvement pouvait être controllé selon différents points d'accès. Grâce à la maitrise du mouvement de projection, la catapulte peut lancer n'importe quel légo et ce sans mouvement de retour brusque. 

 > `le kit de projection`

 ![catapulte](https://imagizer.imageshack.com/v2/800x600q90/922/NlqmhC.jpg)


> `ACTION`

![décollage](https://imagizer.imageshack.com/v2/800x600q90/922/3X4in0.png)

* Vidéo du décollage : [take off](https://youtube.com/shorts/2lgtvRBjNhY)
* Vidéo en slow motion : [slomo](https://youtube.com/shorts/pfuB6SIVjEQ)

