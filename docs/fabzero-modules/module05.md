# 5. Dynamique de groupe et projet final

Ce module est dédié à la gestion de projet et à la réflexion sur une problématique trouvée.


## 5.1 Analyse d'une problématique 

Lors de notre projet final et plus généralement au cours de notre vie, nous seront confrontés à différentes problématiques qu'il nous faudra apprendre à gérer et par la suite à résoudre. Une manière de faire face à ces problématiques se trouve dans les concepts **d'arbre à problèmes **et** d'arbre à solutions**. Ces concepts sont faciles à prendre en main et possèdent des résultats encourageants. Une courte vidéo d'exemple est disponible [ici](https://youtu.be/9KIlK61RInY). 

Dans notre cas, nous devions d'abord cibler un problème et par la suite sa solution en suivant les différentes étapes que je vais décrir ci dessous. 
Le travail qui va suivre a été réalisé en collaboration avec [Emma Dubois (ou Barkardottir)](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/emma.dubois/) et [Lucie Lestienne](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/Lucie.lestienne/).


### 5.1.2 Identifier le problème principale (Tronc)

Après avoir parcouru les [17 Sustainable Development Goals](https://sdgs.un.org/) listés par les nations unies, nous avons décidé de nous intéresser à la problématique de l'accès à l'eau potable en Afrique. En effet, dans le cadre du cours de pollution du mileu physique, une slide nous avait particulièrement marqué:


> Répartition en % des ressources en eau potable et des populations par continents. 

![image eau](https://imagizer.imageshack.com/v2/600x400q70/923/GSkpSu.png)




Cette slide démontre les problèmes liés à l'accès à l'eau potable dans certaines parties du globe. Nous pouvons observer qu'en Europe, malgré une disponiblité en eau plus faible proportionnellement à l'Afrique. La population ne souffre en général pas ou peu du manque d'eau. Un problème qui est au contraire très présent en Afrique et nous allons essayer de comprendre pourquoi. 

Afin de rendre notre problèmatique plus concrète et nos actions plus réalisables, nous avons essayé de cibler un problème précis et nous nous sommes renseignés sur **l'accès à l'eau potable au Sénégal**.

```
L’accès à l’eau potable demeure un problème au Sénégal, pour certaines populations, en particulier celles des quartiers pauvres ainsi que des zones rurales. De nombreuses localités ne disposent pas encore d’eau courante à domicile et s’approvisionnent à partir des puits et bornes fontaines publiques. Jusqu’à fin 2009, un peu plus de 26% de la population rurale s’approvisionnaient encore à partir de sources d’eau non « potabilisée ». En 2015, ce taux se réduit à 14%.

```
* Source : [CNCD](https://www.cncd.be/eau-senegal-interets-prives-bien-public-suez)



### 5.1.3 Identifier les causes (racines)

Après une recherche biblographique sur le sujet, nous avons identifié différentes causes à l'impact plus ou moins équivalent. 


 * **Privatisation** de l'eau potable sous forme d'un partenariat publique-privé
 * **Manque** d'investissement dans les infrastructures 
 * Croissance démographique **exponentielle**
 * **Réchauffement** climatique, impliquant aridité, érosion et ruissellement. 

### 5.1.4 Identifier les conséquences (branches)

Les principales conséquences qui en découlent sont les suivantes : 

* **Maladie hydrique**/ problèmes de santé publique 
* **Instabilité** croissante dans le pays entre les autorités et la population locale 
* Augmentation du **cout** de l'eau 
* Problèmes d'**hygènes** (de nombreux habitants retournent à des moyens basiques d'extractions d'eau comme les puits ou l'irrigation)

### 5.1.4 L'arbre à soucis 

![arbre faché](https://imagizer.imageshack.com/v2/800x600q70/922/2vWXsr.png)

## 5.2 Arbre à objectifs 

> «Chaque problème peut être résolu avec la démarche adéquate.»


Avec l'arbre à objectif, l'idée est de tranformer le problème identifié en un objectif à atteindre pour le solutionner, tout en se donnant les moyens de complir cet objectif. 

### 5.2.1 Problème -> objectif 

Dans notre cas, nous avons considéré qu'une _amélioration des infrastructures d'accès à l'eau_ était un objectif concret et réalisable qui pourrait offrir une solution à notre problématique. 

### 5.2.2 Causes -> Activités 

Afin d'améliorer ces infrastuctures, nous avons pensé à organiser notre plan selon **4** axes principaux:
* A. Sensibilisation globale

L'idée serait de conscientiser le monde (et principalement les jeunes) à notre problématique afin d'élargir le spectre d'entités capables de trouver des solutions. Pour cela, nous pourrons faire appel par exemple à [EYP (European Youth Parliament)](https://eyp.org/). Une organisation dont le but est de faire participer des jeunes venus de toute l'Europe à des séminaires de réfléxions sur certaines thématiques et de trouver des solutions pratiques à celles-ci.
* B. Sensibilsation locale

A travers [Amnesty International](https://www.amnesty.org/fr/), il est possible de lancer des actions de plaidoyer visant les chefs d'état d'un certain pays pour cibler une problématique spécifique. Le but serait alors de déprivatiser l'accès à l'eau afin de transiter vers une politique publique de celle-ci.
* C. Soutien financier  

Des récoltes de fonds organisés par l'ONG [Eau-vives](https://www.cariassociation.org/Organismes/Eau-Vive-Senegal) pourraient être relayées à plus grandes échelle afin de créer un budget conséquent capable de soutenir la construction de nouvelles infrastructures. 
* D. solutions pratiques  

Une façon de trouver des solutions pratique serait de faire appel au réseau des différents Fablab. Comme lors des inondations en Inde, il serait possible de mobiliser plusieurs Fablab dans le monde afin de créer et d'établir des solutions rapides et facilement applicables par les locaux. 

### 5.2.3 Conséquences -> Résultats 
> Les activités ci dessus, si elles sont bien effectuées pourraient donnés lieu aux résultats suivants. 

* **Déprivatisation** de l'eau potable
* Amélioration de l'**hygène** de vie des habitants 
* Augmentation des offres d'**emplois** dans le pays cible 
* Diminution du **coût** de l'eau

### 5.3.4 L'arbre à solutions 

![abre content](https://imagizer.imageshack.com/v2/800x600q70/923/VJWx3b.png)


## 5.3 Formation de groupes et brainsortming 

La séance du 7/3/23 avait pour but de former les groupes pour le projet final et de déjà créer une synergie en mettant les groupes néo-formés face à différentes activités de réfléxions. 

### 5.3.1 L'objet 

La première étape était d'apporter en classe un objet que l'on pouvait lier à une problématique qui nous tenait à coeur. Dans mon cas, j'ai apporté une **citrine**, une variété de Quartz dont la couleur est due aux infimes quantités d'oxydes de fer présents dans le minéral. 


![image](https://imagizer.imageshack.com/v2/600x400q70/924/OuTQ8l.jpg)



J'ai lié cet objet à l'extraction de minerai et plus précisément de **métaux rares**. Pour alimenter la transition numérique que le monde connait actuellemement, il faut des ressources. Certaines de ces ressources sont accessibles, d'autres le sont beaucoup moins. Le terme métaux rares rassemble un ensemble d'éléments dont la production mondiale ne dépasse pas les 500 000 t/an, soit environ 0,02 % de la production minière mondiale. Du fait de leurs caractéristiques physiques et/ou chimiques, ils ont de nombreuses applications dans la haute technologie. Les conséquences de l'extractions de ces éléments sont nombreuses: 
 > Rejet de déchets toxiques, pollution des nappes phréatiques, manque de protection des ouvriers, atteinte à la fertilité des sols, déforestation, pertes de biodiversité, contamination de l'eau, des sols, de l'air ...

Alors que dans certain pays des normes ont été mises en place afin de limiter les dégats de leurs extractions, d'autres ont fait le choix de sacrifier leur environnement au profit de ceux engrengés par ce marché. 

J'ai trouvé cette problématique intéressante car officieuse. Il n'est par exemple pas à l'avantage des entreprises de production/vente de voitures éléctriques que ces problèmes soient exposés à un plus large public que celui qui creuse. Un livre écrit par Guillaume Pitron intitulé _La guerre des métaux rares_ synthétise bien le sujet et est une lecture que je recommande tout particulièrement. 

### 5.3.2 Formation des groupes
![groupes](https://imagizer.imageshack.com/v2/800x600q70/923/EqC9iU.jpg)

Afin de former les groupes, nous nous sommes dirigés vers les personnes possédant un objet présentant des caractéristiques communes avec le notre. [Halil](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/halil-ibrahim.cela/) et [Antoine](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/antoine.trillet/) avaient tout les deux amenés une branche morte pour soulever la thématique de l'écologie et [Eliot](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/eliot.niedercorn/) avait étonnamment aussi pensé à l'extraction des métaux rares comme sujet!

### 5.3.3 Brainsortming

Une fois réuni par groupe, plusieurs activités ont été organisées pour nous permettre de déjà réfléchir à certaines problématique et de travailler notre imagination collective. 

#### A. Mots communs

La première a été de trouver le maximum de concepts/mots étant communs à nos objets respectifs dans un laps de temps de 3 minutes. Nous en avons trouvé 19. 

> Eliot et Antoine, fiers de la performance réalisée 


![yo](https://imagizer.imageshack.com/v2/800x600q70/922/NmFplH.jpg)

#### B.  Thématiques

Nous avons ensuite discuté des thématiques relevées par le premier exercices et les avons placé sur un gradient d'enthousiasme en fonction des gouts et des couleurs de chacuns. 

![y](https://imagizer.imageshack.com/v2/800x600q70/922/KiIXSE.jpg)

#### C.  Problématiques 

Suite aux thématiques choisies, nous avons du réfléchir à des problématiques liées à celles-ci. Un exercice qui s'est révélé particulièrement difficile car nous sommes plus habitué à être dans une optique de recherches de solutions que de problèmes. 

![y](https://imagizer.imageshack.com/v2/800x600q70/923/16i8FN.jpg)

#### D.  Aides externes 

Ensuite, l'un des membres du groupe a été récolter des idées d'autres groupes concernant les problématiques sur nos thématiques. Les 3 autres sont restés pour faire de même avec les émissaires des autres groupes. Voici les idées trouvées par les autres groupes: 

![y](https://imagizer.imageshack.com/v2/800x600q70/922/7aydiR.jpg)

### 5.3.4 Le groupe !

![groupe](https://imagizer.imageshack.com/v2/800x600q70/924/KJTtsn.png)

## 5.4 Dynamique de groupe 

Différents outils afin d'améliorer la dynamique de groupe nous ont été enseignés, je vais en citer 3 qui nous ont particulièrement marqué après discussion. 

> La discussion en question 


![réflexio](https://imagizer.imageshack.com/v2/800x600q70/923/qmnhI4.jpg)


### 5.4.1 Prise de température (processus de décision)

Le groupe a estimé que cette manière de donner son avis ou du moins son ressenti pouvait être efficace et permettrait de guider les discussions ou les débats vers l'essentiel.


### 5.4.2 Répartition des tâches/rôles 

On considère que l'attribution de rôles est important pour le fonctionnement d'un groupe car ceux-ci permettent de répartir les tâches, de gagner du temps et de se situer dans le débat. Les différents rôles répartis ont été : 


* Secrétaire (moi) 
* Animateur 
* Gestionnaire du temps 


Evidemment, ces différents rôles peuvent varier en fonction de l'année. 

### 5.4.3 Météo d'entrée 

Faire une brève introduction personnelle sur la manière dont on se sent avant une réunion ou un débat permet à tous de comprendre et d'intèrpreter le comportement de chacun. Nous allons essayer d'utiliser cet outil car nous pensons qu'il est utile à la discussion et au fonctionnement du groupe car il permet d'éviter l'intèrepretation personnelle de certains comportements au sein du groupe. 

# 6. Conclusion finale

## 6.1 le début de la fin 

En conclusion, ce cours m'a permis à la fois d'étendre mon champ de connaissance en termes d'informatiques, de codage, de gestion de projet mais d'appliquer ces différentes connaissances à la pratique au travers des différents modules.   

## 6.2 Imprimante 3D 

Grâce à cette formation, j'ai découvert la joie de pouvoir transformer des idées abstraites en outils concrets. j'ai appris à coder sur **OPENSCAD** et à utiliser une **imprimante 3D**

## 6.3 Micro-controleur

Ce module m'a permis de me plonger au coeur du fonctionnement de nos outils informatiques à travers l'apprentissage puis l'utilisation de **micro-controleurs**. J'ai pu reprendre les bases que j'avais en **python** et les développer. J'ai suivi une mini-formation youtube afin de comprendre plus le sujet que je devais manipuler et j'ai essayé d'utiliser les différentes fonctions de ces minis-ordinateurs afin d'avoir un aperçu global de l'utilité de ceux-ci. 

## 6.4 CNC 

Grâce au [projet de groupe](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/groups/group-09/), j'ai appris à utiliser **Fusion360** afin de designer des formes spécifiques. Dans notre cas, nous avons fabriqué des prises d'escalades grâce à la **CNC**! Nous avons donc tous du passer par l'apprentissage long et douloureux de cet outil. 

## 6.5 Découpeuse Laser 

A travers le projet, nous avons aussi tenter de fabriquer des prises à la découpeuse laser, je n'ai créé ces prises mais j'ai eu l'occasion d'apprendre à lancer les découpes et d'avoir une vue d'ensemble sur la façon dont fonctionne cet outil. 

## 6.6 Mot de la fin

C'est donc avec beaucoup de sincérité que je recommande cette formation. Au dela des outils et de l'enseignement technique, ce cours m'a permis de rencontrer et de tisser des liens avec de nombreuses personnes. Un coté social que je n'ai retrouvé dans aucun autre cours à l'unniversité. 

