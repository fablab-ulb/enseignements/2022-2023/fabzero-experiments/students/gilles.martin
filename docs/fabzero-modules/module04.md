# 4. Electronic protoyping 

Le but de ce module est d'apprendre à utiliser un microcontroleur afin de prototyper des fonctions utiles grâce à des outils de programmation et des périphériques fournis. 

## 4.1 Microcontroleurs

"Les microcontrôleurs sont des circuits intégrés programmables qui combinent une unité centrale de traitement (CPU), de la mémoire et des périphériques d'entrée/sortie dans un seul et même composant. Ils sont souvent utilisés pour des applications qui nécessitent un contrôle électronique, tels que les systèmes embarqués, les automates industriels, les robots, les capteurs, les contrôleurs de moteurs, les équipements médicaux, les appareils ménagers, les dispositifs portables, les jouets électroniques, etc."


En gros, c'est un outil qui permet d'avoir à disposition les mêmes fonctions qu'un ordinateur mais à plus faible coût. 


Dans le cadre du cours, nous avons reçu le modèle RP2040 : 

<p align="center">
  <img src="https://i.imgur.com/1dVfzPm.png" alt="rp">
</p>

 Un microcontroleur designé par l'entreprise [Raspberry Pi](https://www.raspberrypi.com/documentation/microcontrollers/rp2040.html)



## 4.2 Langages 

Afin de programmer le microcontroleur, plusieurs langages sont disponibles, en voici quelques exemples : 

* Micropython 
* Langage C 
* Arduino 

### 4.2.1 <span style="color:green"> Arduino

C'est le langage le plus abordable, l'interface est pratique et directe, les fonctions et les commandes sont très accessibles et il existe beaucoup de documentation pour appuyer et répondre à de potentielles questions. En gros, c'est le plus facile à prendre en main. Cependant, en simplifiant énormément la programmation et le code il réduit aussi les possibilités qu'offrent les microcontroleurs. 
Exemples et librairies : [Arduino librairie](https://github.com/flrrth/pico-dht20)


### 4.2.2  <span style = "color:yellow"> C 

Langage puissant, efficace, c'est le langage le plus utilisé afin de réalisé les tâches les plus complexes. Il est largement utilisé dans les domaines tels que la fincance, les jeux vidéos ou encore l'industrie. Pour ma part, j'ai fait le choix de ne pas m'embarquer dans ce langage de programmation pour le moment. 
Exemples et librairies : [C librairie](https://github.com/raspberrypi/pico-examples/tree/master/i2c)

### 4.2.3  <span style = "color:blue"> Micropython 

C'est le langage que j'ai décidé d'utiliser. En effet, je possède déjà de l'expérience dans ce langage contrairement aux deux autres grâce au cours d'INFO-F206.
Exemples et librairies : [Micro librairie](https://github.com/flrrth/pico-dht20)


## 4.3 Installation 

Afin d'installer d'utiliser micropython pour commencer à programmer le microcontroleur, il faut d'abord suivre une série d'étapes : 


* https://thonny.org/
* pip install thonny
* launch thonny
* tool > options > interpreter : rp2040
* reboot rp2040 in boot mode (boot button pressed while reset released)
  ** click install or update MicroPython (bottom)
* ok => you’re now talking/interpreting python from the rp2040
* select pico as target (pico and xiao-rp2040 act the same)
* Source : [FablabUlb](https://fablab-ulb.gitlab.io/enseignements/fabzero/electronics/languages/python/)

Une fois ces étapes suivies, on peut brancher le microcontroleur à notre ordinateur grâce au cable USB-C fourni dans notre kit. 

![](attention.png "Attention")

Il est essentiel d'appuyer sur le bouton BOOT lors de l'étape du branchement sinon le microcontroleur ne se synchronisera pas avec votre ordinateur. 

Pour ma part, dès que je rebranche mon micrcontroleur, un message d'erreur s'affiche sur ma console :

<p align="center">
  <img src="https://i.imgur.com/9pqfWu1.png" alt="prbl">
</p>

Je dois alors aller dans `Exécuter`->`configurer l'interpréteur`-> `install or update Micropython` et choisir la version Raspberry Pi Pico. 


## 4.4 Exercice 1 : NeoPixel 

Le but de ce premier exercice est de se familiariser avec l'outil. Un [exemple de code](https://github.com/raspberrypi/pico-micropython-examples/blob/master/pio/pio_ws2812.py) afin de configurer notre lampe led sur le board du microcontroleur était directement disponible et il nous a suffit de l'exécuter sur notre dossier `main.py` qui est le dossier que contient notre microcontroleur. 

J'ai décidé pour ma part d'explorer un peu plus les fonctionnalités des lampes led en suivant un tuto clair et simple disponible sur youtube sur le chaine de <span style = "color:gold"> Paul McWhorter. 

[Lien première vidéo](https://youtu.be/SL4_oU9t8Ss)

Cela m'a permis de comprendre un peu plus en profondeur la manière de coder afin de programmer le microcontroleur et j'ai d'ailleurs créé un mini-circuit éléctrique simple permettant d'allumer et d'éteindre une lampe disposée sur mon breadboard : 

* Lien youtube : [Microcontroler led blinking](https://youtube.com/shorts/p2Jnhkht4Jo?feature=share)
* Le code : 

```
from machine import Pin
from time import sleep 
luz = Pin((25),Pin.OUT)
blink = 0.03
i = 0.01
while True:
    sleep(blink)
    luz.value(1)
    sleep(blink)
    luz.value(0)

```
Dans ce code, on importe de deux librairies (machine,time) des fonctions qui nous seront utiles. On détermine une variable ```luz``` et on spécifie à quelle sortie le cable sera branché. La fonction ```sleep()``` permet de déterminer le temps d'envoi d'une information au microcontroleur. ```Value()``` permet d'activer (1) ou d'éteindre (0) la lampe. 


## 4.5 Exercice 2 : DHT20 - Temperature & RH sensor

Le but de cet exercice est d'utiliser le sensor dht20 afin de pouvoir mesurer la température et l'humidité au sein de la pièce.


> DHT20

> ![sensor](https://i.imgur.com/bwATzrc.jpg)

Il est important de d'abord lire la [datasheet](https://cdn.sparkfun.com/assets/8/a/1/5/0/DHT20.pdf) du sensor afin d'en comprendre ces principales caractéristiques comme la gamme de voltage qu'il supporte (2.2-5.5)V DC, ses performances (sa gamme d'erreur) mais aussi son fonctionnement. 

Une donnée particulièrement utile est son interface qui permet de configurer l'embranchement de celui-ci au board de notre microcontroleur.  

>![inter](https://i.imgur.com/TBnbaFe.png)

Le code pour utiliser le sensor est lui aussi disponible juste ici et les explications pour chaque ligne de code sont écrites dans les paramètres.  

```

// importation des bibliothèques nécéssaire à l'interaction entre le microcontroleur et le capteur

from machine import Pin, I2C
from utime import sleep



from dht20 import DHT20

// définition des broches SDA et SCL

i2c0_sda = Pin(8)
i2c0_scl = Pin(9)

// initialisation de l'interface I2C 

i2c0 = I2C(0, sda=i2c0_sda, scl=i2c0_scl)

// création de l'objet DHT20, la fonction prend 2 arguments: l'adresse du capteur disponible dans la data sheet de celui-ci (chapitre 7.3)

dht20 = DHT20(0x38, i2c0)

// création d'une boucle infinie afin de lire grâce à 'measurements' et d'afficher grâce à 'print' en continu (avec un interval de une seconde) les mesures de température et d'humidité)

while True:
    measurements = dht20.measurements
    print(f"Temperature: {measurements['t']} °C, humidity: {measurements['rh']} %RH")
    sleep(1)

```


* Source : [Github](https://github.com/flrrth/pico-dht20)

Problème, le code ne marche pas car Thonny ne trouve pas ``` DHT20 ``` dans la librairie dht20. Cela s'explique car je ne dispose pas encore de la librairie dht20. 
Une pré étape est alors nécéssaire afin de lancer ce code, il faut d'abord créer un fichier à enregistrer dans Raspberry Pi Pico afin de lui donner l'information, la librairie par laquelle nous allons chercher la fonction DHT20 nécéssaire à la prise de mesure. Le code a inséré dans ce fichier est disponible [ici](https://github.com/flrrth/pico-dht20/blob/main/dht20/dht20.py)

Il est aussi important de bien gèrer le branchement des cables en fonction des Pin choisies dans le code et de l'interface de votre sensor, voila comment j'ai organisé les différents liens : 


<p align="center">
  <img src="https://i.imgur.com/z5LHFi1.png" alt="rp">
</p>

* `VDD`->3V3
* `SDA`->PIN8
* `SCL`->PIN9
* `GND`->GND



## 4.6 Mini Projet : Accelerometer 

Un accéléromètre est un capteur qui mesure l'**accélération** linéaire d'un objet dans un espace tridimensionnel. En utilisant un accéléromètre avec un microcontroleur, on peut détecter les mouvements de l'objet et utiliser ces informations pour contrôler des actions ou des affichages. C'est notamment le concept qu'utilisent les créateurs de téléphones afin de changer l'affichage Horinzontal/Vertical lorsqu'on tourne notre téléphone. 

J'ai personellement hérité du modèle VMA208 : 

<p align="center">
  <img src="https://i.imgur.com/CWYduwE.png" alt="rp">
</p>

J'ai d'abord eu du mal à trouver de la documentation concernant ce modèle, j'ai alors trouvé la nouvelle version de celui-ci, le microcontroleur **MMA8452Q**.

Et voici le code que j'ai utilisé pour faire fonctionner mon accéléromètre, bien aidé par chatgpt car il y'a une tonne de librairie à télécharger pour lire les informations et controler l'accéléromètre et je ne dispose pas forcément de celles-ci.


```

# import des modules nécéssaires; machine pour l'accès au pin, le module time pour attendre des durées spécifiées, le module ustruct pour encoder et décoder des données binaires, et le module utime pour gérer les temps d'attente.

import machine
import time
import ustruct
import utime

# configuration des pins I2C, le microncontroleur doit être configuré pour communiquer avec ce périphérique. On définit aussi la fréquence de communication afin de garantir la fiabilité de la transmission des données 


i2c = machine.I2C(0, scl=machine.Pin(9), sda=machine.Pin(8), freq=400000)

#définition de l'adresse de l'accéléromètre

MMA8452Q_ADDR = 0x1D

# initialisation de MMA8452Q grâce à différents registre qui le mettent en mode actif 

i2c.writeto_mem(MMA8452Q_ADDR, 0x2A, b'\x00') # Set mode to standby
i2c.writeto_mem(MMA8452Q_ADDR, 0x2A, b'\x01') # Set mode to active
i2c.writeto_mem(MMA8452Q_ADDR, 0x0E, b'\x00') # Set data rate to 800 Hz
i2c.writeto_mem(MMA8452Q_ADDR, 0x0F, b'\x00') # Set range to +/-2g

# Lecture des données prises par l'accéléromètre, les données sont lues gràace aux registres spécifiés ci-dessous, on divise ensuite par 1024 afin de convertir les données brutes.


while True:

    accel_data = i2c.readfrom_mem(MMA8452Q_ADDR, 0x01, 6)
    x = ustruct.unpack('<h', accel_data[0:2])[0] / 1024.0
    y = ustruct.unpack('<h', accel_data[2:4])[0] / 1024.0
    z = ustruct.unpack('<h', accel_data[4:6])[0] / 1024.0

    # Affichage des données 
    print("Accelerometer X: {:.2f}".format(x))
    print("Accelerometer Y: {:.2f}".format(y))
    print("Accelerometer Z: {:.2f}".format(z))

    # Temps d'attente entre lectures afin d'éviter le surchargement du microntroleur

    utime.sleep_ms(1000)

```
