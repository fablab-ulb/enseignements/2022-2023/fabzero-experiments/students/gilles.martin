# 1. La base des bases



### 1.1 introduction 

Lors de cette première séance de cours, nous avons été introduit aux différents outils de base qui nous serviraient plus tard lorsque nous devrons entamer notre documentation. Nous avons aussi été introduit à la mentalité _Fablab_ et les objectifs de cette formation nous ont été donnés : 
> Apprendre les bases d'outils numériques puissants afin de pouvoir en explorer d'autres de manière plus autonome .

Une visite des fablab sous forme de chasse aux trésors a aussi été organisé à travers ce [mur collaboratif](https://app.mural.co/t/fablabulb8809/m/fablabulb8809/1675889255251/c92104cd65a4ef55e53d383e2b08f74e7cc1dbfe?invited=true&sender=ud57f89de5b836653f9e52849) de la plateforme Mural. Cela qui nous a permis de nous projeter dans les différents domaines touchés par cette formation.

Le cours est enseigné principalement par TERWAGNE Denis et porte le nom suivant : _How To Make (almost) Any Experiment Using Digital Fabrication_.

# 2. Les outils de bases 
 
 Voici une liste d'exemple des outils enseignés afin de créer une documentation lisible et conforme: 
 * CLI
 * Visual Code
 * Graphicsmagick
 * MKdocs

N'ayant que des connaissances très limitée en informatique (Python et Octave), chaque étape fut laborieuse et je vais essayer de les décrire de la manière la plus claire possible. 

### 2.1 Unix/CLI/GUI

N'ayant pas d'idée claire sur ce qu'est un système d'exploitation, j'ai été me renseigner sur cette page [wikipedia](https://fr.wikipedia.org/wiki/Syst%C3%A8me_d%27exploitation) afin de pouvoir me situer. 
Unix est donc un système d'exploitation, à la même enseigne que Windows. 

Un GUI est un type de logiciel, un _Graphical User Interface_. C'est le type de logiciel le plus couramment utilisé car il est visuel et permet un accès à l'information simple et rapide pour n'importe quel néophyte en informatique. Plus d'info sur sont disponibles sur cette [page web](https://www.ionos.fr/digitalguide/sites-internet/developpement-web/quest-ce-quune-gui/)

Un CLI est un _Command Line user Interface"_. Utiliser des programmes à travers ce genre de logiciel est un outil puissant et c'est cela que nous allons utiliser. 

### 2.2 installation d'un CLI

J'ai téléchargé et installé git bash à travers [ce lien ci](https://git-scm.com/download/win). La démarche pour installer des logiciels est souvent la même. Voici les étapes : 
1. Localiser le lien du téléchargement et lancer l'installation. 
2. Une première copie du logiciel se trouvera sur votre disque dur : 

  ![photo copie logiciel](https://imagizer.imageshack.com/v2/320x240q90/923/GNtYCH.png)

3. Lire et accepter (si cela convient) les termes et conditions. 
4. Lancer le logiciel. 

### 2.3 Les bases de la synthax 

Afin de connaître les bases de la synthax, j'ai utilisé le [tutoriel fablab](https://github.com/Academany/fabzero/blob/master/program/basic/commandline.md) mis à notre disposition et voici les informations principales que j'en ai tirées:

| Commande |  Description    
|-----|-----------------
| `ls`  | liste des dossiers dans le dépot    
| `cd` | Naviguer entre les dossiers   |  
| ```man``` | Pour appeler  l'aide, diminutif de manual |  
| ```pwd```| Afin de se retrouver, affiche le chemin du _filesystem_  | 
| `mkdir`   | Pour créer un dépot | 
| `find`  | Afin de retrouver des fichiers |  

### 2.4 Git set up 

Afin de travailler en groupe sur un même projet, nous utilisons le serveur GitLab. Sur ce serveur, nous partageons un projet commun sur lequel nous pouvons tous travailler. Afin de pouvoir travailler localement sur ce projet, il faut avoir un moyen de s'identifier afin de pouvoir "pousser" les codes écris localement sur le serveur public. Pour cela, il faut se créer des **_SSH keys_**. Cet outil permet au serveur de nous authentifier et nous permet de pouvoir travailler sur notre projet sans devoir constamment se connecter en ligne sur GitLab. 

Le protocole que j'ai suivi pour créer ma clé publique et ma clé privée est [celui-ci](https://docs.gitlab.com/ee/user/ssh.html).

Une fois le protocole terminé, mon accès au serveur est sécurisé, je peux créer un **clone**. 

Cette étape a été particulièrement chronophage pour ma part et je vais essayer de la détailler le plus possible afin d'éviter que l'erreur ne se répète. 

#### 1. étape 
J'ai été sur ma page Gitlab et j'ai séléctionné l'icone "**cloner**" pour copier l'URL (celle qui me permettra de cloner avec les clés SSH)
> icone en <span style = "color:blue"> bleue au dessus à droite

![clone](https://imagizer.imageshack.com/v2/640x480q90/922/exJqPw.png)

#### 2. étape 
J'ai ouvert Git bash et utilisé la commande `git clone`, Git a créé un dossier _nom.prénom_ dans lequel se trouve tous les dossiers présents sur ma page personnelle GitLab.

#### 3. l'erreur 
Un soucis de manipulation m'a fait effacé le dossier Mkdocs et le dossier Index présent dans mon fichier gilles.martin créé par GitLab. Mon clone. Cela m'a donc empêcher de suivre les prochaines étapes et de travailler sur mon site web. Car à chaque manipulation sur le terminal, ce message d'erreur était affiché : 

> _destination path already exists and is not an empty directory_

J'ai donc été cherché la solution sur des forum de discussion et j'ai trouvé une solution simple et efficace. Effacer mon ancien dossier gilles.martin et en recréer un nouveau à travers ce code : 

```
cd ~/   

mkdir code

cd code

git clone (url copié lors de la 1e étape)
```

J'ai donc un nouveau dossier contenant tous les fichiers nécéssaires.

### 2.5 Visual Code 

Visual Code est un éditeur de texte, tout comme bloc-notes ou Workpad mais plus puissant que ceux-ci.
Afin de me faire la main, j'ai utilisé [ce tutoriel](https://www.markdowntutorial.com/)
mais aussi [cette vidéo](https://youtu.be/34_dRW42kYI). 
En voici les commandes principales  : 

|commande|utilité|exemple d'utilisation|autre
|----|----------|--------|-----
|`#`| Titre| # Titre | ## donnera un titre de police plus petite
|`**`| mettre en gras| c'est une é**gras**tinure| ** à gauche et à droite de la partie à surligner
|`_` |écrire en italique|j'adore visiter _l'italique_|_ à gauche et à droite de la partie à italiquer 
|`[testlien](lien)`|lien inline| [yo](https://fr.wiktionary.org/wiki/yo)|
|`![testimage](lien)`|image inline|![yo](https://imagizer.imageshack.com/v2/100x75q90/924/EJENAG.png)
|`>`|paragraphe| > cette phrase est plus importante qu'une autre|
|`*`|liste| * poivrons|
|`Crtl+shift+v`| prévisualisation du travail|
|`[^numéro]`|référence|[^1]|

### 2.6 Le transfert d'information 

Pour actualiser et en quelque sorte sauvegarder les modifications faites sur le fichier sur lequel je travail. J'ai utilisé différentes commandes. 
Tout d'abord j'enregistre mes modifications dans visual code. J'ouvre un terminal et me situe dans le dossier où les modifications ont été faites. 
Je vérifie grâce à la commande **_git status_** si des modifications ont en effet été faites sur mon dossier et je les déplace vers la _staging area_ grâce à **_git add -A_**. Ensuite, j'engage ces changements dans l'historique du projet avec **_git commit -m"commentaire quelconque"_**. Enfin, je 'pousse' ces modifications vers le dossier publique afin qu'elles soient disponibles pour le groupe avec **_git push_**.

>**_voir code ci dessous_**


![image code](https://imagizer.imageshack.com/v2/640x480q90/922/3PHlpp.png)

![image code2](https://imagizer.imageshack.com/v2/640x480q90/923/PwJnKC.png)

>**_schéma synthétique_**

![schema](https://imagizer.imageshack.com/v2/640x480q90/924/HJvYoM.png)


### 2.7 travail sur image 

Afin de pouvoir travailler sur les images qui me seront utiles, j'ai téléchargé GIMP et lu ce [court tutoriel](https://www.gimp.org/tutorials/GIMP_Quickies/) clair et efficace.
A travers ce tuto, on peut apprendre comment compresser ces images et éviter de sur-saturé son espace de stockage de manière inutile (puis c'est mieux pour la planète). 

Cependant je ne peux empêcher la perte un peu trop forte de qualité des images sur mon site. J'essayerai de regler ce problème lors de la prochaine session. 


