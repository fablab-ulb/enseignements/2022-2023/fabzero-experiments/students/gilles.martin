# 2. Création assistée par l'ordi 

Cette semaine fut riche en découvertes et en enseignements, beaucoup de thèmes ont été abordés lors de ce module et je vais essayer de détailler du mieux possible mon processus d'apprentissage ci-dessous  <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-person-workspace" viewBox="0 0 16 16">
  <path d="M4 16s-1 0-1-1 1-4 5-4 5 3 5 4-1 1-1 1H4Zm4-5.95a2.5 2.5 0 1 0 0-5 2.5 2.5 0 0 0 0 5Z"/>
  <path d="M2 1a2 2 0 0 0-2 2v9.5A1.5 1.5 0 0 0 1.5 14h.653a5.373 5.373 0 0 1 1.066-2H1V3a1 1 0 0 1 1-1h12a1 1 0 0 1 1 1v9h-2.219c.554.654.89 1.373 1.066 2h.653a1.5 1.5 0 0 0 1.5-1.5V3a2 2 0 0 0-2-2H2Z"/>
</svg>. 

## 2.1 Computer-Aid Design (CAD)

**CAD** regroupe l'ensemble des logiciels de conception assistée par ordinateur. C'est à dire des logiciels qui permettent la création et la manipulation numérique de formes géométriques. Ces logiciels sont utilisés dans de nombreux domaines, on peut y citer par exemple la mécanique, l'éléctrotechnique ou encore l'architecture. 
Nous avons été introduits à 3 de ces outils/logiciels lors du cours sur le modèle 2, en voici les principales caractéristiques. 

### 2.1.1 Inkscape 
![logo](https://upload.wikimedia.org/wikipedia/commons/thumb/0/0d/Inkscape_Logo.svg/30px-Inkscape_Logo.svg.png)

Inkscape est un logiciel de dessin vectoriel **2D**. C'est à dire que l'image n'est pas formée par des points (pixels) mais plutôt par des explications que l'on fourni au logiciel. C'est un outil simple à maitriser qui permet notamment un accès facile à tout ceux voulant s'essayer au dessin graphique.



### 2.1.2 Openscad

Openscad est un logiciel gratuit et open-source qui permet la réalisation de modèles.
 Ce logiciel est destiné à la réalisation de modèles 3D et possède la caractéristique de ne se baser que sur des codes pour modèliser les solides 3D. Il permet donc un partage d'informations et d'objet 3D assez important car quiconque souhaiterait partager sa création pourrait partager son code et d'autres pourraient l'utiliser afin de crèer un objet propre à leur projet. 
 
 Pour ma part, c'est le logiciel que j'ai le plus utilisé, afin de prendre la main j'ai suivi ce [tutoriel-ci](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero/computer-aided-design/-/blob/master/OpenSCAD.md).
 Il est possible de créer rapidement des formes intéressantes avec seulement quelques lignes de codes 
 > `petit cone`

 ![cone](https://imagizer.imageshack.com/v2/500x260q90/922/EThNXC.png)

 Les différentes commandes sont facilement accessibles sur la la page [cheat sheet](https://openscad.org/cheatsheet/)


### 2.1.3 Freecad 

Lui aussi est un logiciel de dessin vectoriel 3D. Il semble possèder un plus large spectre en termes de formes et de modules qu'openscad et ne se base pas spécialement sur le script pour la réalisation de modèles 3D. Il permet aussi le transfert d'information provenant d'openscad. Cela s'avère pratique si l'on souhaite transfèrer un projet créé sur openscad pour lui apporter des modifications sur Freecad.


## 2.2 Au boulot 

L'idée de ce module était de modéliser un flexlink à travers l'un des outils présenté lors du cours. Les **Flexlinks** sont des composants permettant de construire un large champ de "compliant mechanism". Ces mécanismes flexibles permettent de transmettre une force, un mouvement à travers l'élasticité d'un corps. L'idée ici est de s'entrainer à utiliser les logiciels 3D en en créant un. Les différents **FlexLinks** sont disponibles sur [cette page](https://www.compliantmechanisms.byu.edu/flexlinks).

> _exemple de FlexLink_ 

![flexx](https://static.wixstatic.com/media/f04fcf_baedc3165ed94e44a2a315791bc72963~mv2.gif)

### 2.2.1 L'idée

Pour ma part, j'ai décidé d'essayer de créer quelque chose d'un peu différent afin d'essayer d'apprendre le plus possible par moi même et de profiter de ces nouveaux outils. 
J'ai alors eu l'idée d'essayer de crèer un mini-ressort plat comme première pièce, en voici la toute première esquisse, je vous demande donc d'être un minimum indulgents.
> `1ère esquisse`

![esquisse](https://imagizer.imageshack.com/v2/400x300q90/922/Q3N1gk.png)



 J'ai alors cherché quelques images afin de m'inspirer d'une certaine forme que je pourrais reproduire en flexlink. 
Voici les outils que j'ai trouvé et sur lesquels je me suis basé : 

> `ce genre d'outils`

![ressort](https://imagizer.imageshack.com/v2/320x240q90/923/LxJrFm.png) ![ressort2](https://imagizer.imageshack.com/v2/320x240q90/924/YZS4yZ.png)


### 2.2.2 La construction 


Une fois l'idée plus ou moins claire dans ma tête, je me suis lancé. J'ai d'abord codé les deux parties qui seront reliées aux legos. J'ai ensuite formé le corps de mon ressort en essayant d'être le plus paramétrique possible dans ma démarche même si les premiers codes ressemblaient plus à du bricolage numérique qu'autre choses
.Le premier résultat potable obtenu était le suivant:


![premiere forme](https://imagizer.imageshack.com/v2/640x480q90/922/wzaq7s.png)


Il me restait donc quand même pas mal de pain sur la planche, j'ai donc assemblé les différentes parties ensembles par jeu de transitions mais un problème demeurait, je n'arrivais pas à faire une _différence()_ dans mon code et donc à créer les trous qui permetteraient aux légos de s'insérer dans mon FlexortLink. J'ai donc utilisé le code de [Mats Bourgeois](https://fablab-ulb.gitlab.io/enseignements/2021-2022/fabzero-experiments/students/mats.bourgeois/), un des étudiants de l'année 2021-2022. Un code que j'ai trouvé sur le site [FabLab experiments](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/class-website/) et dont le résultat final avait cette forme là :


![codemat](https://imagizer.imageshack.com/v2/640x480q90/923/uPJMfX.png)

J'ai donc décidé d'utilisé et d'adapté son code afin d'avoir des attaches solides pour mon ressort. J'ai rajouté une entrée et j'ai modifié la longueur et la position des attaches. 

### 2.2.3 Format Lego


Une dernière étape consistait à adapter mon flexort au format légo, voici les différentes contraintes imposées par ce format: 


| Partie  | Taille   | Commentaire |
| ------- | -------- | ------------|
| Rayon insertion embout  | `2.5 mm` | le rayon de l'embout est de 2.4 mm, mais il faut prévoir un mm de plus pour assurer la bonne insertion |
| Epaisseur du flexlink (partie reliante) | `1.5 mm` |  une largeur plus large impliquerait une plus grosse rigidité de la pièce|
| Ecart entre le centre deux trous d'insertion | `8mm` | à respecter pour éviter quelconque problème d'insertion 


### 2.2.4 Format final 
 
 Une fois ces dernières modifications faites, mon tout nouvel outil était fin près à être envoyé dans la salle aux imprimantes. 

 `resultat final`


 ![résultat](https://imagizer.imageshack.com/v2/800x600q90/923/9lvwsz.png)

### 2.2.5 Le code 

```

/*
    * File : Flexor link
    * Author : Martin Gilles
    * Date : 10-03-23
    * Copyright (c) 2023 Martin Gilles
    * Licensed under the MIT License
*/

//definition des paramètres

$fn=150;
l_e=3;
adj=17;
calcul_hyp=(pow(adj,2)+pow(l_e,2));
hyp = sqrt(calcul_hyp);
angle_1=acos(adj/hyp);
joint=hyp/10.5;
n_etages = 6;
height=3;
hradius=4;
radius=2.5;
ecart=8;
width_r = 1.6;
corr = l_e/15;
//formation de la premiere base du ressort

// Code by Mats Bourgeois from "Fixed-Slotted-Beam-Straight" under the MIT License


module base_res(hyp,adj,l_e){
    

    translate([0,adj/2.2,0])
    cube([width_r,adj/2.2,3]);
    translate([l_e-0.1,0,0])
    rotate([0,0,angle_1])
    #cube([width_r,hyp-1,3]);
}
base_res(hyp,adj,l_e);

// formation de la 2e base du ressort

module base_res2(hyp,adj,l_e){
     translate([(n_etages*(2*l_e))+(l_e-1),0,l_e])
     rotate([0,180,0])
base_res(hyp,adj,l_e);
}
base_res2(hyp,adj,l_e);

// formation du cylindre gauche faisant office de joint

module cyl_g(joint){
    translate([joint-1,adj-0.4,0])
    cylinder(height,width_r,width_r);
} 
cyl_g(joint);
// boucle afin d'obtenir tous les joints 
    
    for (i=[(n_etages-corr):l_e*2:(l_e*2)*n_etages]){
    cyl_g(joint+i);
} 
// meme idée pour les cylindres droits

module cyl_d(l_e){
    translate([l_e+0.7,0,0])
    cylinder(height,width_r,width_r);}
    
    cyl_d(l_e);
    
for (i2 =[0:n_etages:(n_etages-1)*n_etages]){
    cyl_d(l_e+i2);} 
    
// creation de 2 boucles pour obtenir les parties centrales du ressort
    
for (i=[3*l_e:n_etages:n_etages*5+l_e]){
    translate([i,0,0])
    rotate([0,0,angle_1])
    #cube([width_r,hyp,3]);
}

for (i=[l_e:(2*l_e):(n_etages-1)*n_etages]){
    
    translate([i,0,0])
    rotate([0,0,-angle_1])
    #cube([width_r,hyp,3]);
}

//formation des deux bases permettant d'y inserer les légos
    
module base_1(height,hradius,radius){
    difference(){
        hull(){
            cylinder(height, hradius, hradius);
            translate([0,hradius+ecart/2,0])
            cylinder(height, hradius, hradius);
            translate([0,hradius-5*ecart/3,0])
            cylinder(height, hradius, hradius);
        }

  union(){
           translate([0,ecart,-0.5])
            cylinder(height+1, radius, radius);
            translate([0,0,-0.5])
            cylinder(height+1, radius, radius);
            translate([0,-ecart,-0.5])
            cylinder(height+1, radius, radius);
  }
  }}
translate([-hradius,9.5,0])
base_1(height,hradius,radius);

module base_2(height,hradius,radius){
    translate([((n_etages+1)*(2*l_e))-corr,9.5,0])
    base_1(height,hradius,radius);
}
base_2(height,hradius,radius);

```
### 2.2.6 Remarque

Bien que le résultat final soit efficace, je n'ai pas réussi à paramétriser parfaitement mon code. Cela implique que la seule modification des paramètres n'est pas suffisante pour avoir une maitrise totale sur l'objet 3D. 


## 2.3 Licence creative commons

Les Licences creative commons sont un outil qui permet à l'auteur d'une création de conserver ses droits sur son oeuvre tout en permettant la libre circulation de celle-ci. C'est à dire que les créateurs laisse le public copier, distribuer ou modifier leurs oeuvres tout en gardant leurs droits dessus. Cet outil permet le partage de connaissance tout en respectant la propriété intelectuelle de chacun. 

Il existe plusieurs types de licences possèdant différents types de contrats et de logos, pour en savoir plus, j'ai lu cette [page-web](https://fr.wikipedia.org/wiki/Licence_Creative_Commons)

J'ai personnellement utlisé la licence MIT qui permet une utilisation très libre du code, à condition que la notice de copyright et la licence soient conservées et que toute modification apportée au logiciel soit clairement indiquée.

